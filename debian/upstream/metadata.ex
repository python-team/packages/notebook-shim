# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/notebook-shim/issues
# Bug-Submit: https://github.com/<user>/notebook-shim/issues/new
# Changelog: https://github.com/<user>/notebook-shim/blob/master/CHANGES
# Documentation: https://github.com/<user>/notebook-shim/wiki
# Repository-Browse: https://github.com/<user>/notebook-shim
# Repository: https://github.com/<user>/notebook-shim.git
